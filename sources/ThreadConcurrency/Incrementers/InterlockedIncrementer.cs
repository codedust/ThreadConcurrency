﻿// ThreadConcurrency
// Copyright (C) 2018 Dust in the Wind
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Threading;

namespace DustInTheWind.ThreadConcurrency.Incrementers
{
    /// <summary>
    /// This class will create a number of threads that will concurrently increment an integer field.
    /// The threads are synchronized using the <see cref="Interlocked"/> static class.
    /// </summary>
    internal class InterlockedIncrementer : IIncrementer
    {
        private readonly int threadCount;
        private readonly int incrementCount;

        private long value;

        public long Value => value;

        public InterlockedIncrementer(int threadCount, int incrementCount)
        {
            if (threadCount <= 0) throw new ArgumentOutOfRangeException(nameof(threadCount));
            if (incrementCount <= 0) throw new ArgumentOutOfRangeException(nameof(incrementCount));

            this.threadCount = threadCount;
            this.incrementCount = incrementCount;
        }

        public void Run()
        {
            List<Thread> threads = new List<Thread>();

            for (int i = 0; i < threadCount; i++)
            {
                Thread thread = new Thread(o =>
                {
                    for (int j = 0; j < incrementCount; j++)
                        Interlocked.Increment(ref value);
                });

                thread.Start();

                threads.Add(thread);
            }

            foreach (Thread thread in threads)
                thread.Join();
        }
    }
}